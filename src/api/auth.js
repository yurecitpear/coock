import httpClient from "./httpClient.js";

const url = ''

export const authApi = {
  
  doLogin(data) {
    return httpClient.post(`login`, {
      email: data.email,
      password: data.password
    }).then(({data}) => {
      const user = JSON.stringify(data)
      localStorage.setItem('user', user)

      document.cookie = `role=${data.role}; max-age=3600; path=/`;
      document.cookie = `token=${data.token}; max-age=3600; path=/`;

      return data;
    }).catch((err) => {
      throw err;
    })
  },
  
  doRegister(data) {
    return httpClient.post(`register`, {
      name: data.name,
      email: data.email,
      phoneNumber: data.phoneNumber,
      password: data.password,
    }).then(({data}) => {
      const user = JSON.stringify(data)
      localStorage.setItem('user', user)

      document.cookie = `role=${data.role}; max-age=3600; path=/`;
      document.cookie = `token=${data.token}; max-age=3600; path=/`;

      return data
    })
    .catch((err) => {
      throw err;
    })
  },

  doLogout() {
    localStorage.clear();
    document.cookie = "role=; expires=0; path=/;";
    window.location.href = '/auth/login'
    location.reload()
  }
  
}