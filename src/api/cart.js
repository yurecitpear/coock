import httpClient from "./httpClient.js";

const url = 'orders'

export const cartApi = {

  getCartInStorage() {
    if (localStorage.getItem('cart')) {
      const obj = JSON.parse(localStorage.getItem('cart'))
      return obj
    } else {
      const obj = {
        "description": null,
        "name": null,
        "shipping_address": null,
        "phonenumber": null,
        "products": []
      }
      return obj
    }
  },
  addCartInStorage(obj) {
    const objStorage = JSON.stringify(obj)
    localStorage.setItem('cart', objStorage);
  },

  


  addProduct(obj, count) {
    const cart = this.getCartInStorage()
    
    const product = cart.products.find(item => item.id === obj.id)
    if(product) {
      cart.products.map((item) => {
        if (item.id === obj.id) {
          item.count += count
          return item
        }
      })
    } else {
      const product = {
        id: obj.id,
        name: obj.name,
        count: 1,
        category: obj.category.name,
        main_image: obj.main_image.short_url,
        price: obj.price,
      }
      cart.products.push(product)
    }

    this.addCartInStorage(cart) 
  },

  minusProduct(obj, count) {
    const cart = this.getCartInStorage()
    const product = cart.products.find(item => item.id === obj.id)

    if(product) {
      cart.products.map((item) => {
        if (item.id === obj.id) {
          item.count -= count
        }
        if (item.count < 1) {
          console.log(item.count)
          return cart.products = cart.products.filter((el) => el.id !== obj.id)
        } else {
          return item
        }
      })
    }

    this.addCartInStorage(cart) 
  },


  delProduct(obj) {
    const cart = this.getCartInStorage()
    cart.products = cart.products.filter((el) => el.id !== obj.id)
    this.addCartInStorage(cart) 
  },
  
  clearCart() {
    
  },

}