import httpClient from "./httpClient.js";

const url = 'images'

export const imageApi = {
  delImage(id) {
    try {
      return httpClient.delete(`${url}/${id}`)
      .then(({ data }) => {
        return data 
      })
    } catch(err) {
      console.log(err)
    }
  },
}

