import httpClient from "./httpClient.js";

const url = 'categories'

export const categoryApi = {
  getAll() {
    try {
      return httpClient.get(`${url}`)
      .then(({ data }) => {
        return data 
      });
    } catch(err) {
      console.log(err)
    }
  },
  
  getCatById(id) {
    try {
      return httpClient.get(`${url}/${id}`)
      .then(({ data }) => {
        return data 
      });
    } catch(err) {
      console.log(err)
    }
  },

  createCategory(nameCat) {
    try {
      return httpClient.post(`${url}`, {
        name: nameCat
      })
      .then(({ data }) => {
        return data 
      })
    } catch(err) {
      console.log(err)
    }
  },
  
  updateCategory(nameCat, id) {
    try {
      return httpClient.patch(`${url}/${id}`, {
        name: nameCat
      })
      .then(({ data }) => {
        return data 
      })
    } catch(err) {
      console.log(err)
    }
  },

  delCategory(id) {
    try {
      return httpClient.delete(`${url}/${id}`)
      .then(({ data }) => {
        return data 
      })
    } catch(err) {
      console.log(err)
    }
  },

}