
export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  mode: 'universal',
  srcDir: 'src/',
  dir: {
    pages: 'pages',
    layouts: 'layouts',
    components: 'components',
    plugins: 'plugins',
    store: 'store',
    static: 'assets'
  },

  server: {
    host: process.env.NODE_ENV === 'production' ? process.env.SERVER_HOST : 'localhost',
    port: process.env.NODE_ENV === 'production' ? process.env.SERVER_PORT : 3000
  },

  router: {
    middleware: ['authenticated'],
  },

  head: {
    title: 'coock',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      { src: '/js/core.min.js', body: true },
      { src: '/js/script.js', body: true }
    ],
  }, 

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/bootstrap.css',
    '@/assets/css/fonts.css',
    '@/assets/css/style.css',
    '@/assets/css/my-style.css',
  ],
  

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '~/plugins/vuelidate' },
    { src: '~/plugins/vue-notification', mode: 'client' },
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxt/http'
  ], 

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/dotenv',
    // '@types/vuelidate',
  ],   
  
  publicRuntimeConfig: {
    apiUrl: process.env.URL_PATH
    // apiUrl: process.env.URL_PATH || 'http://45.12.236.128:3000',
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    withCredentials: true,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
