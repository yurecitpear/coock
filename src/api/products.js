import httpClient from "./httpClient.js";

const url = 'products'

export const productApi = {
  getAll() {
    try {
      return httpClient.get(`${url}`)
      .then(({data}) => {
        return data
      })
    } catch(err) {
      throw err
    }
  },

  getAllAll() {
    try {
      return httpClient.get(`${url}?lim=200`)
      .then(({data}) => {
        return data
      })
    } catch(err) {
      throw err
    }
  },

  getById(id) {
    return httpClient.get(`${url}/${id}`)
      .then(({ data }) => {
        return data
      })
      .catch((err) => {
        throw err
      })
  },
  
  getAllByCategory(id) {
    return httpClient.get(`${url}?c=${id}`)
      .then(({data}) => {
        return data
      })
      .catch((err) => {
        if (err.response.status === 500) {
          return ''
        } else {
          throw err
        }
      })  
  },

  async addPhotoGallery(id, images) {
    if (images.length === 0) {
      return;
    }

    for (var i = 0; i < images.length; i++) {
      const imageData = new FormData();
      let file = images[i];
      imageData.append(`image`, file);
      await httpClient({
        method: "patch",
        url: `${url}/${id}/image`,
        data: imageData,
        headers: { "Content-Type": "multipart/form-data" },
      })
    }
  },

  async createProduct(obj) {
    const formData = new FormData()
    formData.append(`name`, obj.name);
    formData.append(`category_id`, obj.category_id);
    formData.append(`description`, obj.description);
    formData.append(`price`, obj.price);
    formData.append(`image`, obj.image);
    
    try {
      const { data } = await httpClient({
        method: "post",
        url: `${url}`,
        data: formData,
        headers: { "Content-Type": "multipart/form-data" },
      });
      await this.addPhotoGallery(data.message, obj.images);
      return data.data;
    } catch (err) {
      throw err;
    }
  },

  async addPhoto(image) {
    const imageData = new FormData()
    imageData.append(`image`, image)

    try {
      const data = await httpClient({
        method: "post",
        url: `images`,
        data: imageData,
        headers: { "Content-Type": "multipart/form-data" },
      })
      return data.data.id
    } catch (err) {
      throw err;
    }
  },

  async updateProduct(obj) {
    const formData = new FormData()
    formData.append(`name`, obj.name);
    formData.append(`category_id`, obj.category_id);
    formData.append(`description`, obj.description);
    formData.append(`price`, obj.price);
    
    const data = {
      name: obj.name,
      category_id: obj.category_id,
      description: obj.description,
      price: obj.price,
    }

    if (obj.image) {
      const resp = await this.addPhoto(obj.image)
      data.main_image_id = resp
    }

    try {
      return httpClient.patch(`${url}/${obj.id}`, data)
      .then(({data}) => {
        return data.data
      })
    } catch(err) {
      throw err
    }
  },


  delProduct(id) {
    try {
      return httpClient.delete(`${url}/${id}`)
      .then(({ data }) => {
        return data 
      })
    } catch(err) {
      console.log(err)
    }
  },

  
}