import httpClient from "./httpClient.js";

const url = 'orders'

export const ordersApi = {
  getAll() {
    return httpClient.get(`${url}`)
    .then(({data}) => {
      return data
    })
    .catch((err) => {
      throw err
    })
  },

  getById(id) {
    return httpClient.get(`${url}/${id}`)
    .then(({ data }) => {
      return data
    })
    .catch((err) => {
      throw err
    })
  },

  saveOrder(data) {
    return httpClient.post(`${url}`, data)
    .then(({data}) => {
      return data
    })
    .catch((err) => {
      throw err
    })
  },

  

}