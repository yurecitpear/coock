
export default function ({ route, redirect, req}) {
  const meta = route.meta.find((meta) => 'role' in meta)
  const role = meta ? meta.role : undefined

  let userRoles = null;

  if (process.server && req && req.headers) {
    const cookie = req.headers.cookie;
    if (cookie) {
      const userRoleCookie = cookie.split(';').find(c => c.trim().startsWith('role='));
      if (userRoleCookie) {
        userRoles = userRoleCookie.split('=')[1];
      }
    }
  } else if (process.client && localStorage) {
    const user = JSON.parse(localStorage.getItem('user'));
    userRoles = user ? user.role : null;
  }
  
  if (role && role.length > 0 && !role.includes(userRoles)) {
    redirect('/auth/login');
  }
}