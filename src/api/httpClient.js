import axios from "axios";

function logOut() {
  localStorage.clear();
  document.cookie = "role=; expires=0; path=/;";
  document.cookie = "token=; expires=0; path=/;";
  window.location.href = '/auth/login'
  location.reload()
}

const config = {
  baseURL: 'https://kharchofood.ru/api'
  // baseURL: process.env.URL_PATH_API || "http://45.12.236.128:4002/api/"

}

const httpClient = axios.create(config);
if (process.client) {
  const user = JSON.parse(localStorage.getItem('user'))
  if (user) {
    var token = user.token
  } else {
    var token = ''
  }
}

if (token) {
  httpClient.interceptors.request.use(
    (config) => {
      config.headers.Authorization = `Bearer ${token}`
      return config  
    }
  )
  
  httpClient.interceptors.response.use(function (response) {
    return response
  }, function (error) {
    const err = error.response.status
  
    const originalRequest = error.config
    if (originalRequest && !originalRequest._isRetry) {
      originalRequest._isRetry = true
  
      if(err === 401) {
        console.log('Неавторизован')
        logOut() 
      }
      if(err === 403) {
        console.log('Нет доступа')
      }
      if(err === 404) {
        console.log('Не найдено')
      }
    }
    return Promise.reject(error)
  })
}




export default httpClient
